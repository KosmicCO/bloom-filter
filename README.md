# CS 421 Mini-Project 2: Hash, Bloom, and Filter
 
Project by Cruz Barnum.
The GitLab repository can be found [here](https://gitlab.com/KosmicCO/bloom-filter).
Note that the debug version was used for the following tests; however, the release versions are included as binaries for faster/more efficient tests.

 ## Designing a Hash Function

 The hash functions that are available are
 - Sha256
 - Default Rust
 - Seeded Pseudorandom
 - Moddulus

Each can be found in the `hashers.rs` folder.
Note that each hasher should also work as a normal hasher in rust, since they implement the `Hasher` trait, which stipulates that each `Hasher` should work over an arbitrary number of bytes.
If only `u64`s are hashed over though, they should work as expected as if they only used numerics.

### Hash Testing

The binary `hash_testing` runs a bucket hashing test over the 4 included hashers.
Each has a default value, so they do not need to be explicitly defined.
The binary can be run using the command
```Rust
./hash_testing -b [buckets] -n [num hashes] -h [hashers]
```
Other information can also be found by running
```Rust
./hash_testing -h
```

The test returns the min bucket, max bucket, expected bucket, standard deviation of the buckets, the toal collisions (usually will not be helpful with overfull buckets), and the time to compute all the hashes.

The test hashes each hasher on the `u64`s `0..n`.
If the hashers model a random function well, we would except that there is no correlation between inputs and output, so the "sameness" of the inputs should be irrelevant.
As such, the sameness of the input can be thought of as a non-randomized workload, i.e. a workload with very low entropy.
We run tests over the given parameters:

| Trial # | Buckets | Num Hashes | Hashers |
| :--: | --: | --: | --: |
| 1 | 100  | 1     | 10000 |
| 2 | 1000 | 10    | 10000 |
| 3 | 100  | 10000 | 1     |
| 4 | 1000 | 10000 | 10    |
| 5 | 10000 | 10000| 1  |
| 6 | 10000 | 1000 | 10   |

The tests are modeled around how a random function would operate.
When the multiplication of the number of hasher `n` and the hashers `h` divided by the number of buckets `b`, we expect that the standard deviation be the same.
As such, we expect that trials grouped 1-4 and trials grouped 5-6 have the same standard deviation if the hashers are actually random (which they aren't).
Trials 1-4 simply test the standard deviation aspect and how the modeled randomness changes between different hashes of the same hasher and different hashers of the same family.
Trials 5-6 express how a family behaves when having a situation where the entirety of the buckets could theoretically be perfectly filled up.

### Hash Results

The results are given.
Note that the debug version was used so that the times would be larger than normal.
Release versions of rust usually speed things up by 30x.

#### `Sha256`

| Trial | Min | Max | Expected | Stand. Dev. | Collisions | Time (ms) |
| :--: | --: | --: | --: | --: | --: | --: |
| 1 | 77 | 124 | 100. | 9.21 | 9900 | 177 |
| 2 | 67 | 130 | 100. | 9.61 | 99000 | 1440 |
| 3 | 76 | 131 | 100. | 9.54 | 9900 | 161 |
| 4 | 73 | 139 | 100. | 10.17 | 99000 | 1484 |
| 5 | 0 | 6 | 1.00 | 1.00 | 3691 | 149 |
| 6 | 0 | 7 | 1.00 | 1.00 | 3651 | 149 |

#### `Rust Default`

| Trial | Min | Max | Expected | Stand. Dev. | Collisions | Time (ms) |
| :--: | --: | --: | --: | --: | --: | --: |
| 1 | 75 | 122 | 100. | 8.67 | 9900 | 3 |
| 2 | 72 | 131 | 100. | 9.63 | 99000 | 34 |
| 3 | 77 | 122 | 100. | 8.50 | 9900 | 2 |
| 4 | 67 | 132 | 100. | 9.92 | 90000 | 30 |
| 5 | 0 | 8 | 1.00 | 1.01 | 3687 | 3 |
| 6 | 1 | 8 | 1.00 | 1.00 | 3636 | 3 |

#### `Seeded Pseudorandom`

| Trial | Min | Max | Expected | Stand. Dev. | Collisions | Time (ms) |
| :--: | --: | --: | --: | --: | --: | --: |
| 1 | 78 | 123 | 100. | 9.79 | 9900 | 17 |
| 2 | 71 | 128 | 100. | 10.12 | 99000 | 175 |
| 3 | 69 | 126 | 100. | 9.26 | 9900 | 15 |
| 4 | 67 | 133 | 100. | 10.38 | 99000 | 147 |
| 5 | 0 | 7 | 1.00 | 1.01 | 3731 | 17 |
| 6 | 0 | 6 | 1.00 | 1.00 | 3684 | 17 |

#### `Modulus`

| Trial | Min | Max | Expected | Stand. Dev. | Collisions | Time (ms) |
| :--: | --: | --: | --: | --: | --: | --: |
| 1 | 72 | 129 | 100. | 10.33 | 9900 | 16 |
| 2 | 71 | 132 | 100. | 10.22 | 99000 | 137 |
| 3 | 99 | 102 | 100. | 0.60 | 9900 | 15 |
| 4 | 88 | 111 | 100. | 4.00 | 99000 | 137 |
| 5 | 0 | 2 | 1.00 | 0.70 | 2461 | 16 |
| 6 | 0 | 6 | 1.00 | 0.94 | 3463 | 15 |

### Analysis

Note that the first 3 hashers are pretty consistently around the expected standard deviation for each trial (i.e. `sqrt(expected) = sqrt(n*h/b)`).
Also note that the `Sha256` hasher should be considered an actual random function for our purposes, since cryptographically it should be indistinguishable from any given actual random function.
As such, if our code somehow is able to show weird results/patterns about `Sha256`, then we not only win a Turing award for our monumental discovery in cryptography, but also destabalize the banking and private messaging systems.

That said, we move on to the `Modulus` hasher.
The Modulus hasher performs similarly to the other 3 hashers in trials 1-2; however, note that the implementation of `Modulus` hasher stipulates that a family be given seeds from a pseudorandom function (here its simply rust's default hasher).
Since trials 1-2 have a large number of hashers and low hashes per hasher, it is to be expected that the high entropy introduced by the randomized seeds will correct for the very pattern-heavy hashes demonstrated in trials 3-6.
The `Modulus` hashers appears to hve the property that it will be able to fill up much of the space very compactly, i.e. low maximum bucket usage and low collision rates.
This suggests that we might be able to devise an adversarial workload which have many collisions by simply having each input be a multiple of some value; however, for random workloads, it is able to smooth it out effectively.

Only having discussed the standard deviations and seeming patterns, note that from an information theory perspective, it is better to use a hasher which more closely models a truely random function (such as the `Sha256` hasher); however, there are more considerations.

Note that the `Sha256` hasher takes around 40x more time than the default hasher to compute, while the other hashers take around 4x longer.
This might be partially due to the fact that the default hasher is specifically optimized for usage in rust, while the others were implemented without optimization considerations.
That being said, the `Sha256` Hasher takes 10x longer than the other hashers implemented as part of the assignment.
This is the main reasons that cryptographic hash functions are not used in non-cryptographic settings most of the time.
In fact, most cryptographic hash functions are usually designed to take a lot of time to compute and not be optimizable to actively reduce the efficacy of bruteforce attacks.

From the consideration of time and standard deviation, we note that the default is the winner unsuprisingly since rust has tested out multiple hash functions, inculding SipHash 2-4 algorithms.
The most recent version of rust uses an unspecified hashing algorithm which is also not guaranteed to stay the same across releases, but it seems to perform well.

## Implementing Bloom Filters

Included in this project are 2 bloom filters; a regular bloom filter and a multi k bloom filter.
Each bloom filter is able to report statistics regarding the probability of accepting a general element that was not added to the filter explicitely (i.e. the false positive rate).

The set of statistics we will use fall into 3 categories:
- Calculated False Positive
- Reported False Positive
- Empirical False Positive

The Calculated False Positive (CFP) is the false positive rate estimated before using the filter, but knowing the load and set sizes in advance.


The Reported False Positive (RFP) is the false positive rate estimated after all elements in the load have been inserted into the filter.
The RFP is calculated using the number of bits in the filter set to `true`, thus is is more accurate than the CFP, which simply uses the expected number of bits which are `true`.

The Empirical False Positive (EFP) is the false positive rate found through empiracle testing.
We should find that the RFP is the closest to this value, followed closely by the CFP.

### Multi K Bloom Filter

The traditional bloom filter treats every item at the same rate, i.e. every value as the same false positive rate.
For datasets where we cannot get further information about inserted items in advance, this is a pretty good system to use; however, if we do have more information or even different rates at which we would like to limit elements in the same set, we can use a multi k bloom filter to separate these elements.

Say that we would like to have a bloom filter over some set `S` to be able to refer to cached data.
Furthermore, lets say that the elements in `S` take some scaling computation times.
For this example, lets say there are two groups of elements such that the first group makes up 90% of `S` and only takes 1ms to recompute while the second group makes up the remaining 10%, but takes 100s to recompute if not already cached (100000x more time!).
We also stipulate that we can figure out in advance which group a given element is a part of.
If we used a traditional bloom filter, the false positive rates for either group of elements would be the same, which means that in order to store the expensive items accurately, we have to store all the items accurately, which is undesirable since we would need a larger bloom filter or more hashes on all items.

Instead of having a fixed `k` that each item must hash with, we can include a function with the bloom filter which decides how many hashers a given element should use, which brings us to the multi k bloom filter.
For the given example, the included function might give the cheaper values a `k` of 2 so that they are stored inaccurately, but give the expensive values a `k` of 8, so that their false positive rate is very very low.

The multi k bloom filter, like the regular bloom filter, gives the CFP and RCP rates; however, it have 2 varieties of the function for calculating the expected general statistics (i.e. over the assumed probability distribution of the element classes) or the statistics for elements of a given `k` class (i.e. only for elements which map to using `k` hashers).
As such, we will see in the tester that each class of statistics (CFP, RFP, and EFP) are broken up into statistics for the general, small `k`, and large `k` false positive rates.

### Calculated False Positive Rates

We give the calculations to find the CFP rates.
Let `n` be the size of the bloom filter, `m` be the number of unique elements added to the bloom filter initially, `k` be the number of hashers used, and `d_i` be the probability of an element with `k` class `i` (used by multi k bloom filter).

#### Regular Bloom Filter

We start by first finding the expected number of bits in the filter set to `true`.
The probability that a bit is set to `true` after a single hash is `1/n`.
The number of hashes done initially on the bloom filter is `k * m`.
As such, the probability that a given bit will be set to true after the `k * m` hashes is
```
p = 1 - (1 - 1/n)^(k * m)
```
Using a binomial distribution, we have that the expected number of bits turned on over the number of bits is
```
(n * p) / n = p
```
We then compute the probability that a non-inserted element is reported as being in the set by the filter (i.e. the `k` hashes are in the `p` probable part of the filter) with
```
p^k = (1 - (1 - 1/n)^(k * m))^k
```
This function is calculated by the function `percent_accept_calc_single`.

#### Multi K Bloom Filter

We start by finding the probability that a bit is still `false` after the hashes from elements of the class `i`.
Note that any sums or producs used enumerated the `i` or `j` indices, so that they do not have to be written.
The probability is the same as the one found previously, except that the number of elements in a class `i` are `i * m * d_i`, thus we have
```
q_i = (1 - 1/n)^(i * m * d_i)
```
We then find that the probability that a bit will still be `false` after all the hashes are over is
```
q = ∏ p_i = ∏ (1 - 1/n)^(i * m * d_i)
```
We then have that the probability that a bit will be set to `true` is
```
p = 1 - q = 1 - ∏ (1 - 1/n)^(i * m * d_i)
```
Note that the expected binomial value given `p` as a fraction of `n` is still `p`, as we found it previously.
The probability that an element from a given class `j` will then be
```
p^j = (1 - ∏ (1 - 1/n)^(i * m * d_i))^j
```
This value is calculated by the multi k bloom filter with the function `percent_accept_calc_multi`.

We can find the general probability than an element drawn at random from the non-inserted elements will be accepted as
```
∑ d_j * p^j = ∑ d_j * (1 - ∏ (1 - 1/n)^(i * m * d_i))^j
```
which is calculated using the function `percent_accept_calc_multi_general`.

### Reported False Positive Rates

Note that the RFP rates use the same equations as above, but since the bloom filter has been filled already, the probability `p` used above can simply be determined directly by counting the percent of bits set to `true` in the filter.

For the regular bloom filter, this will be reported by the function call `self.percent_accept`.
For the multi k bloom filter, this will be reported for specific `k` classes with the function `self.percent_accept` and for general elements with the function `self.percent_accept_general`.

## Bloom Filter Testing

The bloom filter testing takes in the table size `n`, set size `s`, initial elements number `r`, number of elements to find the EFP `t`, small `k`, large `k` `lk`, and the probability that an element should use the large `k` `dk`.

To see usage, run the command
```
./bloom_testing -h
```

The test finds the CFP, RFP, and EFP of the bloom filter with `k` equal to the small `k`, the bloom filter with `k` set to the large `k`, and the multi k bloom filter with a function which decides between the small and large `k` with a probability of `dk` of choosing the large `k`.
The CFP statistics of every variety is computed first with the range `r`.
The RFP statistics of every variety computed after adding the first `r` elements in the range `0..r`.
Since the test uses the default rust hasher, we are using the range for simplicity without effecting element distributions.
The EFP statistics of every variety is computed by counting the number of elements accepted from outside the range.

Here are example results from the default parameters:

| Filter | Statistic | CFP % | RFP % | EFP % |
| :--: | :--: | --: | --: | --: |
| Reg `k=2` | General | 15.48 | 15.72 | 15.76 |
| Reg `k=8` | General | 2.55 |2.50 | 2.49 |
| Multi K | General | 20.59 | 20.70 | 20.73 |
| Multi K | `k=2`   | 22.85 | 22.97 | 23.00 |
| Multi K | `k=8`   |  0.28 |  0.28 |  0.27 | 

## Analysis

We first note that the CFP is accurate to 1% while the RFP is accurate to 0.05%, which is very good.
We can accurately predict the false positive rates for both filters given the arbitrary parameters.
Furthermore, the regular bloom filter is tested twice for each value of `k`.
Note that for `k=8` that the false positive rate is much lower, but the size of the filter is doubled compaired to the `k=2` or multi k bloom filter tests.
Even given the extra space, the regular bloom filter with double the size and `k=8` still has a false positive rate of 10x more for the rare items than the multi k bloom filter.
The only thing the multi k bloom filter sacrifices is 4.5% more false positive for the non rare elements than in the regular bloom map with the same filter size and `k=2`, which is fine especially given the example that we discussed previously.