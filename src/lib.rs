#![feature(build_hasher_simple_hash_one)]

pub mod bloom;
pub mod hash_family;
pub mod hashers;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
