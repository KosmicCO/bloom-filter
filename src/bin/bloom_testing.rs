#![feature(build_hasher_simple_hash_one)]
#![feature(fn_traits)]

use std::{
    collections::hash_map::DefaultHasher,
    hash::{BuildHasher, BuildHasherDefault, Hasher},
    rc::Rc,
};

use bloom_filter::{
    bloom::{
        percent_accept_calc_multi, percent_accept_calc_multi_general, percent_accept_calc_single,
        BloomFilter, MultiKBloomFilter,
    },
    hash_family::{BuildHasherFamily, BuildHasherFamilyDefault},
    hashers::Sha256Hasher,
};
use rand::prelude::*;
use structopt::StructOpt;

#[derive(Debug, StructOpt, Clone, Copy)]
#[structopt(
    name = "bloom_testing",
    about = "Tests the mechanics and false positives rate of bloom filters."
)]
struct Opt {
    /// Size of the bloom filter table.
    #[structopt(short = "b", long, default_value = "4096")]
    table_size: usize,

    /// Size of the bloom filter table using the larger k.
    #[structopt(short = "c", long, default_value = "8192")]
    large_table_size: usize,

    /// Max integer to be considered part of the set when testing.
    #[structopt(short, long, default_value = "1000000000")]
    set_size: u64,

    /// k value input for traditional bloom filter.
    #[structopt(short, default_value = "2")]
    k: usize,

    /// Larger k value used in testing the multi k bloom filter.
    #[structopt(short, long, default_value = "8")]
    lk: usize,

    /// Distribution between small and large k values in the multi k bloom filter.
    /// Probability of using the larger k for a given element.
    #[structopt(short, long, default_value = "0.1")]
    dk: f64,

    /// Range of number to put into the bloom filter initially.
    /// Since the test uses Rust's default hasher, it should be fine to use the range of numbers to ensure no collisions between inserted values.
    #[structopt(short, long, default_value = "1024")]
    range: u64,

    /// Range of values drawn sequentially from outside the initialized range to empirially find the false positive rate.
    /// Since the test uses Rust's default hasher, it should be fine to use the range of numbers to ensure no collisions between inserted values.
    #[structopt(short, long, default_value = "1000000")]
    test: u64,
}

pub fn main() {
    let opt = Opt::from_args();

    if opt.table_size == 0 {
        println!("Table size must be greater than 0.");
        return;
    }

    if opt.large_table_size == 0 {
        println!("Table size must be greater than 0.");
        return;
    }

    if opt.table_size >= opt.large_table_size {
        println!("Large table size must be greater than small table size.");
        return;
    }

    if opt.k == 0 {
        println!("k must be greater than 0.");
        return;
    }

    if opt.k >= opt.lk {
        println!("lk must be greater than k.");
        return;
    }

    if opt.dk < 0.0 || opt.dk > 1.0 {
        println!("Distribution of k values for multi k bloom filter must be a valid probability.");
        return;
    }

    if opt.set_size < opt.range + opt.test {
        println!("The set size must be no smaller than the range and test set size combined.");
    }

    println!(
        "Running tests with table size {}, large table size {}, set size {}, initial elements {}, and test set size {}.",
        opt.table_size, opt.large_table_size, opt.set_size, opt.range, opt.test
    );
    println!(
        "Will use small k of {}, large k of {}, and k distribution of {}.",
        opt.k, opt.lk, opt.dk
    );

    let mut rng = rand::thread_rng();

    // Regular bloom filter tests.

    println!("\nRunning tests on regular bloom filter with k = {} and table size {}:", opt.k, opt.table_size);

    let mut def_bloom = BloomFilter::new(
        BuildHasherFamilyDefault::<BuildHasherDefault<DefaultHasher>>::default_seeded(rng.gen()),
        opt.k,
        opt.table_size,
    );

    println!(
        "Calculated false positive rate: {}",
        percent_accept_calc_single(opt.table_size, opt.k, opt.range)
    );

    for i in 0..opt.range {
        def_bloom.insert(&i);
    }

    println!(
        "Reported false positive rate:   {}",
        def_bloom.percent_accept()
    );

    let mut count = 0;
    for i in 0..opt.test {
        if def_bloom.contains(&(i + opt.range)) {
            count += 1;
        }
    }

    println!(
        "Empirical false positive rate:  {}",
        count as f64 / (opt.test as f64)
    );

    // Regular bloom filter tests with k = lk.

    println!("\nRunning tests on regular bloom filter with k = {} and table size {}:", opt.lk, opt.large_table_size);

    let mut def_bloom = BloomFilter::new(
        BuildHasherFamilyDefault::<BuildHasherDefault<DefaultHasher>>::default_seeded(rng.gen()),
        opt.lk,
        opt.large_table_size,
    );

    println!(
        "Calculated false positive rate: {}",
        percent_accept_calc_single(opt.large_table_size, opt.lk, opt.range)
    );

    for i in 0..opt.range {
        def_bloom.insert(&i);
    }

    println!(
        "Reported false positive rate:   {}",
        def_bloom.percent_accept()
    );

    let mut count = 0;
    for i in 0..opt.test {
        if def_bloom.contains(&(i + opt.range)) {
            count += 1;
        }
    }

    println!(
        "Empirical false positive rate:  {}",
        count as f64 / (opt.test as f64)
    );

    // Multi k bloom filter tests.

    println!("\nRunning tests on multi k bloom filter with table size {}:", opt.table_size);

    let build_hasher =
        BuildHasherFamilyDefault::<BuildHasherDefault<DefaultHasher>>::default_seeded(rng.gen())
            .generate_single(opt.lk + 1);

    let cloned_build_hasher = build_hasher.clone();

    fn kfunc(opt: Opt, build_hasher: &impl BuildHasher, i: &u64) -> usize {
        let hash =
            (build_hasher.hash_one(i) & 0xFFFF_FFFF_FFFF) as f64 / (0x1_0000_0000_0000u64 as f64);
        if hash < opt.dk {
            opt.lk
        } else {
            opt.k
        }
    }

    let mut multi_bloom = MultiKBloomFilter::new(
        BuildHasherFamilyDefault::<BuildHasherDefault<DefaultHasher>>::default_seeded(rng.gen()),
        opt.table_size,
        move |i| kfunc(opt, &cloned_build_hasher, i),
        opt.lk,
    );

    let mut k_dist = vec![0f64; opt.lk];
    k_dist[opt.k - 1] = 1.0 - opt.dk;
    k_dist[opt.lk - 1] = opt.dk;

    println!(
        "Calculated false positive rate:       {}",
        percent_accept_calc_multi_general(opt.table_size, &k_dist, opt.range)
    );
    println!(
        "Calculated k = {} false positive rate: {}",
        opt.k,
        percent_accept_calc_multi(opt.table_size, &k_dist, opt.k, opt.range)
    );
    println!(
        "Calculated k = {} false positive rate: {}",
        opt.lk,
        percent_accept_calc_multi(opt.table_size, &k_dist, opt.lk, opt.range)
    );

    for i in 0..opt.range {
        multi_bloom.insert(&i);
    }

    println!(
        "\nReported false positive rate:         {}",
        multi_bloom.percent_accept_general(&k_dist)
    );
    println!(
        "Reported k = {} false positive rate:   {}",
        opt.k,
        multi_bloom.percent_accept(opt.k)
    );
    println!(
        "Reported k = {} false positive rate:   {}",
        opt.lk,
        multi_bloom.percent_accept(opt.lk)
    );

    let mut count_k = 0;
    let mut num_k = 0;
    let mut count_lk = 0;
    let mut num_lk = 0;

    for i in 0..opt.test {
        let is_lk = kfunc(opt, &build_hasher, &(i + opt.range)) == opt.lk;
        if is_lk {
            num_lk += 1;
        } else {
            num_k += 1;
        }
        if multi_bloom.contains(&(i + opt.range)) {
            if is_lk {
                count_lk += 1;
            } else {
                count_k += 1;
            }
        }
    }

    println!(
        "\nEmpirical false positive rate:        {}",
        (count_k + count_lk) as f64 / (opt.test as f64)
    );
    println!(
        "Empirical k = {} false positive rate:  {}",
        opt.k,
        (count_k) as f64 / (num_k as f64)
    );
    println!(
        "Empirical k = {} false positive rate:  {}",
        opt.lk,
        (count_lk) as f64 / (num_lk as f64)
    );
}
