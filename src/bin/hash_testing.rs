#![feature(build_hasher_simple_hash_one)]

use std::{
    collections::hash_map::DefaultHasher,
    hash::{BuildHasher, BuildHasherDefault},
    time::Instant,
};

use bloom_filter::{
    hash_family::{BuildHasherFamily, BuildHasherFamilyDefault},
    hashers::{BuildModHasherFamily, BuildSeedRandHasherFamily, Sha256Hasher},
};

use rand::prelude::*;
use structopt::StructOpt;

#[derive(Debug, StructOpt, Clone, Copy)]
#[structopt(
    name = "hash_testing",
    about = "Tests the collision rates of a few hash families."
)]
struct Opt {
    /// Number of buckets to test for each method.
    #[structopt(short, long, default_value = "10000")]
    buckets: usize,

    /// Number of elements from the family to test for collisions.
    #[structopt(short, long, default_value = "10")]
    hashers: usize,

    /// Number of hasher to take for each hash in a family, hashing 0 through n for each.
    #[structopt(short, long, default_value = "100000")]
    num: usize,
}

/// Tests the hash family by giving certain statistics about collisions.
/// Returns tuple of min, max, total collisions, and number of empty buckets respectively.
/// Note that at least one of min or number of empty buckets must be 0.
fn test_hash_family(family: impl BuildHasherFamily, opt: Opt) {
    if opt.buckets == 0 {
        println!("No Tests could be conducted with no buckets.");
        return;
    }
    let mut buckets: Vec<usize> = vec![0; opt.buckets];
    let hashers = family.generate_family(opt.hashers);
    let mut collisions = 0u64;
    let start = Instant::now();
    for h in hashers {
        for i in 0u64..opt.num as u64 {
            let ind = (h.hash_one(i) % opt.buckets as u64) as usize;
            if buckets[ind] > 0 {
                collisions += 1;
            }
            buckets[ind] += 1;
        }
    }
    let millis = start.elapsed().as_millis();

    println!(
        "Min bucket uses:         {}",
        *buckets.iter().min().unwrap()
    );
    println!(
        "Max bucket uses:         {}",
        *buckets.iter().max().unwrap()
    );

    if opt.buckets > 1 {
        let expected: f64 = buckets
            .iter()
            .map(|c| *c as f64 / (opt.buckets as f64))
            .sum::<f64>();
        let variance: f64 = buckets
            .iter()
            .map(|c| (*c as f64 - expected).powi(2))
            .sum::<f64>()
            / ((opt.buckets - 1) as f64);
        let variance = variance.sqrt();
        println!("Expected bucket uses:    {}", expected);
        println!("Variance bucket uses:    {}", variance);
    }

    println!("Total collisions:        {}", collisions);
    println!("Elapsed Time:            {}ms", millis);
}

pub fn main() {
    let opt = Opt::from_args();

    println!("Running tests with {} buckets, {} hashes per hasher, and {} hashes per family.", opt.buckets, opt.num, opt.hashers);

    let mut rng = rand::thread_rng();
    println!("\nTesting Sha256 hash implementation:");
    test_hash_family(
        BuildHasherFamilyDefault::<BuildHasherDefault<Sha256Hasher>>::default_seeded(rng.gen()),
        opt,
    );

    println!("\nTesting default rust hash implementation:");
    test_hash_family(
        BuildHasherFamilyDefault::<BuildHasherDefault<DefaultHasher>>::default_seeded(rng.gen()),
        opt,
    );

    println!("\nTesting seeded random hasher:");
    test_hash_family(BuildSeedRandHasherFamily::new(rng.gen()), opt);

    println!("\nTesting modulus hasher:");
    test_hash_family(BuildModHasherFamily::new(rng.gen()), opt);
}
