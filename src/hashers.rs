
use sha2::{Sha256, Digest};

use crate::hash_family::BuildHasherFamily;

use std::{hash::{Hasher, BuildHasherDefault, BuildHasher}, collections::hash_map::DefaultHasher, ops::BitXor};

const DEF_SEED: u64 = 0x5B9E_5E6C_18D7_3381; // Generated using a website for getting randomness.

/// The hasher which takes chunks of 8 bytes, computes a u64 from the bytes, and outputs `r(s+x)` for some seed `s`.
pub struct SeedRandHasher{
    seed: u64,
    output: u64,
    unprocessed: Vec<u8>,
    bhd: BuildHasherDefault::<DefaultHasher>, // This will probably be a 0 byte type.
}

impl SeedRandHasher {

    /// Creates a new instance of the hasher from the given seed.
    pub fn new(seed: u64) ->  Self {
        Self{
            seed,
            output: 0u64,
            unprocessed: Vec::new(),
            bhd: BuildHasherDefault::default(),
        }
    }

    /// Computes `r(s + x)`
    fn seed_hash(&self, x: u64) -> u64 {
        self.bhd.hash_one(self.seed.wrapping_add(x + 1)).bitxor(self.output)
    }
}

impl Default for SeedRandHasher {
    fn default() -> Self {
        Self::new(DEF_SEED) 
    }
}

impl Hasher for SeedRandHasher {
    fn finish(&self) -> u64 {
        if self.unprocessed.len() == 0 {
            return self.output
        }

        let mut bytes = self.unprocessed.clone();
        bytes.resize(8, 0u8);
        let mut num_bytes = [0u8; 8];
        (&mut num_bytes).copy_from_slice(&bytes[0..8]);
        self.seed_hash(u64::from_le_bytes(num_bytes))
    }

    fn write(&mut self, bytes: &[u8]) {
        // Sort of inefficient way of appending, but I dont want to deal with iterators here.
        let mut bytes = Vec::from(bytes);
        let mut unprocessed = self.unprocessed.clone();
        unprocessed.append(&mut bytes);
        let bytes = unprocessed;

        let n = bytes.len() / 8;
        for i in 0..n {
            // Finishes and xors with output.
            let mut num_bytes = [0u8; 8];
            (&mut num_bytes).copy_from_slice(&bytes[i * 8..i * 8 + 8]);
            self.output = self.seed_hash(u64::from_le_bytes(num_bytes))
        }
        let left = bytes.len() - (n * 8);
        self.unprocessed = bytes.iter().cloned().rev().take(left).rev().collect();
    }
}

/// BuildHasher implementation for XORHashers.
/// Stores the seed.
pub struct BuildSeedRandHasher(u64);

impl BuildSeedRandHasher {

    /// Creates a new instance.
    pub fn new(seed: u64) -> Self {
        Self(seed)
    }
}

impl BuildHasher for BuildSeedRandHasher {
    type Hasher = SeedRandHasher;
    fn build_hasher(&self) -> Self::Hasher {
        Self::Hasher::new(self.0)
    }
}

impl Default for BuildSeedRandHasher {
    fn default() -> Self {
        Self::new(DEF_SEED)
    }
}

/// A [`BuildHasherFamily`](crate::hash_family::BuildHasherFamily) for the [`BuildXORHasher`] so that the seeds are consistent and set appropriately.
pub struct BuildSeedRandHasherFamily(u64);

impl BuildSeedRandHasherFamily {
    pub fn new(seed: u64) -> Self {
        Self(seed)
    }
}

impl BuildHasherFamily for BuildSeedRandHasherFamily {
    type BuildHasher = BuildSeedRandHasher;

    fn generate_single(&self, index: usize) -> Self::BuildHasher {
        let mut hasher = BuildHasherDefault::<DefaultHasher>::default().build_hasher(); // Using the default hasher as a pseudorandom function.
        hasher.write_u64(self.0);
        hasher.write_usize(index);
        Self::BuildHasher::new(hasher.finish())
    }
}



const MOD_PRIME: u128 = (1 << 64) - 59u128; // Largest 64-bit prime from https://primes.utm.edu/lists/2small/0bit.html .
const DEF_A: u64 = 0x0247_1c6b_c9c4_84c9;
const DEF_B: u64 = 0x4102_8a1a_4f5b_4c9b; 

/// The hasher which takes chunks of 8 bytes and computes `a * x + b mod p`.
pub struct ModHasher{
    a: u64,
    b: u64,
    output: u64,
    unprocessed: Vec<u8>,
}

impl ModHasher {

    /// Creates a new instance of the hasher from the given seed.
    pub fn new(a: u64, b: u64) ->  Self {
        Self{
            a,
            b,
            output: 0u64,
            unprocessed: Vec::new(),
        }
    }

    /// Computes `ax + b mod p`
    fn mod_hash(&self, x: u64) -> u64 {
        let n = (self.a as u128) * (x as u128) + (self.b as u128); // a * x + b. it is converted to u128 to prevent overflow.
        ((n % MOD_PRIME) as u64).bitxor(self.output)
    }
}

impl Default for ModHasher {
    fn default() -> Self {
        Self::new(DEF_A, DEF_B) 
    }
}

impl Hasher for ModHasher {
    fn finish(&self) -> u64 {
        if self.unprocessed.len() == 0 {
            return self.output
        }

        let mut bytes = self.unprocessed.clone();
        bytes.resize(8, 0u8);
        let mut num_bytes = [0u8; 8];
        (&mut num_bytes).copy_from_slice(&bytes[0..8]);
        self.mod_hash(u64::from_le_bytes(num_bytes))
    }

    fn write(&mut self, bytes: &[u8]) {
        // Sort of inefficient way of appending, but I dont want to deal with iterators here.
        let mut bytes = Vec::from(bytes);
        let mut unprocessed = self.unprocessed.clone();
        unprocessed.append(&mut bytes);
        let bytes = unprocessed;

        let n = bytes.len() / 8;
        for i in 0..n {
            // Finishes and xors with output.
            let mut num_bytes = [0u8; 8];
            (&mut num_bytes).copy_from_slice(&bytes[i * 8..i * 8 + 8]);
            self.output = self.mod_hash(u64::from_le_bytes(num_bytes))
        }
        let left = bytes.len() - (n * 8);
        self.unprocessed = bytes.iter().cloned().rev().take(left).rev().collect();
    }
}

/// BuildHasher implementation for XORHashers.
/// Stores the seed.
pub struct BuildModHasher(u64, u64);

impl BuildModHasher {

    /// Creates a new instance.
    pub fn new(a: u64, b:u64) -> Self {
        Self(a, b)
    }
}

impl BuildHasher for BuildModHasher {
    type Hasher = ModHasher;
    fn build_hasher(&self) -> Self::Hasher {
        Self::Hasher::new(self.0, self.1)
    }
}

impl Default for BuildModHasher {
    fn default() -> Self {
        Self::new(DEF_A, DEF_B)
    }
}

/// A [`BuildHasherFamily`](crate::hash_family::BuildHasherFamily) for the [`BuildXORHasher`] so that the seeds are consistent and set appropriately.
pub struct BuildModHasherFamily(u64);

impl BuildModHasherFamily {
    pub fn new(seed: u64) -> Self {
        Self(seed)
    }
}

impl BuildHasherFamily for BuildModHasherFamily {
    type BuildHasher = BuildModHasher;

    fn generate_single(&self, index: usize) -> Self::BuildHasher {
        let mut hasher = BuildHasherDefault::<DefaultHasher>::default().build_hasher(); // Using the default hasher as a pseudorandom function.
        hasher.write_u64(self.0);
        hasher.write_usize(index);
        let a = hasher.finish();
        hasher.write_u64(DEF_SEED);
        let b = hasher.finish();
        Self::BuildHasher::new(a, b)
    }
}

pub struct Sha256Hasher(Sha256);

impl Default for Sha256Hasher {
    fn default() -> Self {
        Self(Sha256::new())
    }
}

impl Hasher for Sha256Hasher {
    fn finish(&self) -> u64 {
        let hc = self.0.clone();
        let mut num_bytes = [0u8; 8];
        (&mut num_bytes).copy_from_slice(&hc.finalize()[0..8]);
        u64::from_be_bytes(num_bytes)
    }

    fn write(&mut self, bytes: &[u8]) {
        self.0.update(bytes);
    }
}

#[cfg(test)]
mod tests {
    use std::hash::BuildHasher;

    use crate::hash_family::BuildHasherFamily;

    use super::{BuildSeedRandHasherFamily, DEF_SEED};


    /// A problem that was discovered was that the output of all the hashers of the family on input 0 would be the same, regardless of the seed.
    #[test]
    fn test_first_neq_zero() {
        let hashers = BuildSeedRandHasherFamily::new(DEF_SEED).generate_family(2);
        assert_ne!(hashers[0].hash_one(0usize), hashers[1].hash_one(0usize));
    }
}