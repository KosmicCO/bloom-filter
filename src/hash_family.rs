use std::hash::{BuildHasher, Hasher};
use std::rc::Rc;

/// Builds a [`std::hash::Hasher`] from an index that draws from a hashing family.
pub trait BuildHasherFamily {
    type BuildHasher: BuildHasher;

    /// Generates `num` [`Self::BuildHasher`].
    /// Should be consistent across calls throughout the lifetime of the object.
    /// Calls to [`generate_single`](Self::generate_single) for `index` in the range `0..num` should produce the same hash functions in the same order.
    fn generate_family(&self, num: usize) -> Vec<Self::BuildHasher> {
        (0..num)
            .into_iter()
            .map(|i| self.generate_single(i))
            .collect()
    }

    /// Generates the [`Self::BuildHasher`] associated with `index`.
    /// Should be consistent across calls throughout the lifetime of the object.
    fn generate_single(&self, index: usize) -> Self::BuildHasher;
}

/// An encapsulator of [`BuildHasher`](std::hash::BuildHasher)s that are instantiated with an index to create a family.
#[derive(Clone)]
pub struct MemberBuildHasher<B: BuildHasher> {
    index: usize,
    seed: u64,
    build_hasher: Rc<B>,
}

impl<B: BuildHasher> BuildHasher for MemberBuildHasher<B> {
    type Hasher = B::Hasher;

    fn build_hasher(&self) -> Self::Hasher {
        let mut hasher = self.build_hasher.build_hasher();
        hasher.write_u64(self.seed);
        hasher.write_usize(self.index);
        hasher
    }

    fn hash_one<T: std::hash::Hash>(&self, x: T) -> u64
    where
        Self: Sized,
    {
        let mut hasher = self.build_hasher.build_hasher();
        hasher.write_u64(self.seed);
        hasher.write_usize(self.index);
        x.hash(&mut hasher);
        hasher.finish()
    }
}

pub struct BuildHasherFamilyDefault<B: BuildHasher> {
    build_hasher: Rc<B>,
    seed: u64,
}

impl<B: BuildHasher> BuildHasherFamilyDefault<B> {

    /// Creates a new [`FamilyBuildHasher`].
    pub fn new(build_hasher: B) -> Self {
        Self {
            build_hasher: Rc::new(build_hasher),
            seed: 0,
        }
    }

    /// Creates a new [`FamilyBuildHasher`].
    pub fn new_seeded(build_hasher: B, seed: u64) -> Self {
        Self {
            build_hasher: Rc::new(build_hasher),
            seed,
        }
    }
}

impl<B: BuildHasher + Default> BuildHasherFamilyDefault<B> {
    pub fn default_seeded(seed: u64) -> Self {
        Self {
            build_hasher: Rc::new(B::default()),
            seed,
        }
    }
}

impl<B: BuildHasher> BuildHasherFamily for BuildHasherFamilyDefault<B> {
    type BuildHasher = MemberBuildHasher<B>;

    fn generate_single(&self, index: usize) -> Self::BuildHasher {
        Self::BuildHasher{
            index,
            seed: self.seed,
            build_hasher: self.build_hasher.clone(),
        }
    }
}

impl<B: BuildHasher + Default> Default for BuildHasherFamilyDefault<B> {
    fn default() -> Self {
        BuildHasherFamilyDefault::new(B::default())
    }
}