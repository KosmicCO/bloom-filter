use std::{
    cmp::{max, min},
    hash::{BuildHasher, Hash},
    marker::PhantomData,
};

use bitvec::prelude::*;

use crate::hash_family::BuildHasherFamily;

/// Implements a bloom filter which uses multiple hashes from a hash familty.
pub struct BloomFilter<B: BuildHasher> {
    builders: Vec<B>,
    filter: BitVec,
    mask: u64,
}

impl<B: BuildHasher> BloomFilter<B> {
    /// Creates a new [`BloomFilter`](Self) from a given hash family, the number of hashes with `hashers` to to for checking and inserting, and the bit-table size.
    pub fn new<F: BuildHasherFamily<BuildHasher = B>>(
        family: F,
        hashers: usize,
        table_size: usize,
    ) -> Self {
        let mask = (table_size as u64).next_power_of_two() - 1;
        Self {
            builders: family.generate_family(hashers),
            filter: bitvec![0; table_size],
            mask,
        }
    }

    /// Finds the k hashes.
    /// Note that if the mask of the hash exceeds the length of the bitmap, it will be modded by the bitmap length.
    fn multi_hash(&self, item: &impl Hash) -> Vec<usize> {
        self.builders
            .iter()
            .map(|b| {
                let hash = b.hash_one(item) & self.mask;
                if hash >= self.filter.len() as u64 {
                    (hash % self.filter.len() as u64) as usize
                } else {
                    hash as usize
                }
            })
            .collect()
    }

    /// Inserts a new item into the set.
    pub fn insert(&mut self, item: &impl Hash) {
        let hashes = self.multi_hash(item);
        for i in hashes {
            self.filter.set(i, true);
        }
    }

    /// Returns whether the item is in the set, returns false positives.
    pub fn contains(&self, item: &impl Hash) -> bool {
        let hashes = self.multi_hash(item);
        for i in hashes {
            if !self.filter[i] {
                return false;
            }
        }
        true
    }

    /// Returns the probability that an item will be accepted for the specific [`BloomFilter`].
    pub fn percent_accept(&self) -> f64 {
        let count = self
            .filter
            .iter()
            .map(|b| if *b { 1usize } else { 0usize })
            .sum::<usize>() as f64;
        let s_perc = count / (self.filter.len() as f64);
        s_perc.powi(self.builders.len() as i32)
    }
}

/// Returns the probability that an item will be accepted given idealized variables.
pub fn percent_accept_calc_single(table_size: usize, k: usize, m: u64) -> f64 {
    let n = table_size as f64;

    let s_perc = 1f64 - f64::powi(1f64 - 1f64 / n, (k * m as usize) as i32);
    s_perc.powi(k as i32)
}

/// Implements a bloom filter which uses multiple hashes from a hash familty along side the ability to change the number of hashers used per item in accordance with an included function.
pub struct MultiKBloomFilter<B: BuildHasher, I: Hash, K: Fn(&I) -> usize> {
    builders: Vec<B>,
    filter: BitVec,
    kfunc: K,
    max_k: usize,
    mask: u64,
    phantom: PhantomData<I>, // Zero sized type
}

impl<B: BuildHasher, I: Hash, K: Fn(&I) -> usize> MultiKBloomFilter<B, I, K> {
    /// Creates a new [`MultiKBloomFilter`](Self) from a given hash family, the number of hashes with `hashers` to to for checking and inserting, and the bit-table size.
    pub fn new<F: BuildHasherFamily<BuildHasher = B>>(
        family: F,
        table_size: usize,
        kfunc: K,
        max_k: usize,
    ) -> Self {
        let mask = (table_size as u64).next_power_of_two() - 1;
        Self {
            builders: family.generate_family(max_k),
            filter: bitvec![0; table_size],
            mask,
            kfunc,
            max_k,
            phantom: PhantomData,
        }
    }

    /// Finds the k hashes.
    /// Note that if the mask of the hash exceeds the length of the bitmap, it will be modded by the bitmap length.
    fn multi_hash(&self, item: &I) -> Vec<usize> {
        let k = max(min((self.kfunc)(item), self.max_k), 1);
        self.builders
            .iter()
            .take(k)
            .map(|b| {
                let hash = b.hash_one(item) & self.mask;
                if hash >= self.filter.len() as u64 {
                    (hash % self.filter.len() as u64) as usize
                } else {
                    hash as usize
                }
            })
            .collect()
    }

    /// Inserts a new item into the set.
    pub fn insert(&mut self, item: &I) {
        let hashes = self.multi_hash(item);
        for i in hashes {
            self.filter.set(i, true);
        }
    }

    /// Returns whether the item is in the set, returns false positives.
    pub fn contains(&self, item: &I) -> bool {
        let hashes = self.multi_hash(item);
        for i in hashes {
            if !self.filter[i] {
                return false;
            }
        }
        true
    }

    /// Returns the probability that an item will be accepted for the specific [`BloomFilter`].
    pub fn percent_accept_general(&self, k_dist: &Vec<f64>) -> f64 {
        let count = self
            .filter
            .iter()
            .map(|b| if *b { 1usize } else { 0usize })
            .sum::<usize>() as f64;
        let s_perc = count / (self.filter.len() as f64);
        k_dist
            .iter()
            .enumerate()
            .map(|(ik, d)| s_perc.powi((ik + 1) as i32) * d) // Pr[element accepted | k]Pr[k]
            .sum()
    }

    /// Returns the probability that an item will be accepted for the specific [`BloomFilter`].
    pub fn percent_accept(&self, k: usize) -> f64 {
        let count = self
            .filter
            .iter()
            .map(|b| if *b { 1usize } else { 0usize })
            .sum::<usize>() as f64;
        let s_perc = count / (self.filter.len() as f64);
        s_perc.powi(k as i32)
    }
}

/// Returns the probability that an item will be accepted given idealized variables and assuming that the k value of an item is independent of its likelyhood to be included in the filter.
pub fn percent_accept_calc_multi(table_size: usize, k_dist: &Vec<f64>, k: usize, m: u64) -> f64 {
    let n = table_size as f64;

    let s_perc = 1f64
        - k_dist
            .iter()
            .enumerate()
            .map(|(ik, d)| f64::powf(1f64 - 1f64 / n, (ik + 1) as f64 * (m as f64) * d)) // Computes the distributional probabilitiesf for each k.
            .fold(1.0, |a, s| a * s);
    s_perc.powi(k as i32)
}

/// Returns the probability that an item will be accepted given idealized variables and assuming that the k value of an item is independent of its likelyhood to be included in the filter.
pub fn percent_accept_calc_multi_general(table_size: usize, k_dist: &Vec<f64>, m: u64) -> f64 {
    let n = table_size as f64;

    let s_perc = 1f64
        - k_dist
            .iter()
            .enumerate()
            .map(|(ik, d)| f64::powf(1f64 - 1f64 / n, (ik + 1) as f64 * (m as f64) * d)) // Computes the distributional probabilitiesf for each k.
            .fold(1.0, |a, s| a * s);
    k_dist
        .iter()
        .enumerate()
        .map(|(ik, d)| s_perc.powi((ik + 1) as i32) * d) // Pr[element accepted | k]Pr[k]
        .sum()
}

mod tests {

    use super::BloomFilter;
    use crate::hash_family::BuildHasherFamilyDefault;

    use std::collections::hash_map::DefaultHasher;
    use std::hash::BuildHasherDefault;

    use rand::prelude::*;
    use rand_pcg::Pcg64;

    /// Tests that the probability that an item will be accepted
    #[test]
    fn retention() {
        let seed = 82394809771293423u64;
        let mut rng = Pcg64::seed_from_u64(seed);
        let mut set = BloomFilter::new(
            BuildHasherFamilyDefault::<BuildHasherDefault<DefaultHasher>>::default(),
            4,
            100000,
        );
        for _ in 0..4000 {
            set.insert(&rng.next_u64());
        }
        let mut rng = Pcg64::seed_from_u64(seed);
        for _ in 0..4000 {
            assert!(set.contains(&rng.next_u64()));
        }
    }
}
